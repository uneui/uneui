module.exports = {
	env: {
		browser: true,
		es2021: true,
		node: true,
	},
	plugins: ["@typescript-eslint", "vue", "prettier"],
	extends: [
		"plugin:vue/vue3-recommended",
		"@nuxtjs/eslint-config-typescript",
		"plugin:prettier/recommended",
	],
	parser: "vue-eslint-parser",
	parserOptions: {
		ecmaVersion: "latest",
		parser: "@typescript-eslint/parser",
		sourceType: "module",
	},
	rules: {
		"@typescript-eslint/no-unused-vars": "off",
		"vue/multi-word-component-names": "off",
		"no-console": "off",
	},
};
